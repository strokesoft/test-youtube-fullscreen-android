import * as React from 'react';
import { Text, View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import WebView from 'react-native-android-fullscreen-webview-video';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';
// import MyListItem from './components/MyListItem'

class MyListItem extends React.PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.id);
  };

  render() {
    const textColor = this.props.selected ? 'red' : 'black';
    return (
      <View>
        <Text>Text</Text>
      </View>
      // <TouchableOpacity onPress={this._onPress}>
      //   <View>
      //     <Text style={{color: textColor}}>{this.props.title}</Text>
      //   </View>
      // </TouchableOpacity>
    );
  }
}

export default class App extends React.Component {

  // myView = React.createRef();

  // componentDidMount () {
  //   this.myView.current.focus()
  // }

  getYoutubeIframe = (videoId: number) => {

  return (
    "<iframe src='https://www.youtube.com/embed/" +
    videoId +
    "?controls=1&fs=1&showinfo=0&rel=0&autoplay=1' width='560' height='315' frameborder='0' allow='autoplay; encrypted-media'  scrolling='no' allowfullscreen='allowfullscreen' style='position:absolute;top:0;left:0;width:100%;height:100%;overflow:hidden;'></iframe>"
  );
}

getWebview = () => {
  return (
    <WebView
      // source={{ uri: 'https://www.youtube.com/embed/DBXH9jJRaDk' }}
      source={{ html: this.getYoutubeIframe('DBXH9jJRaDk') }}
      style={{
        width: '100%',
        height: 200,
        alignSelf: 'stretch',
        /*borderTopLeftRadius: 6 * initialScale,
        borderTopRightRadius: 6 * initialScale*/
      }} />
  )
}

_renderItem = ({item}) => {
  if ((Math.random() * 10) + 1 > 7) {
    return (
      <View style={{borderWidth: 1}}>
        <Text>{item.key}</Text>
          {this.getWebview()}
      </View>
    )
  }else{
    return (
      <View style={{borderWidth: 1}}>
        <Text>{item.key}</Text>
        <Text>asdf</Text>
        <Text>asdf</Text>
        <Text>asdf</Text>
        <Text>asdf</Text>
        <Text>asdf</Text>
        <Text>asdf</Text>
        <Text>asdf</Text>
        <Text>asdf</Text>
      </View>
    )
  }
}

getList = () => {
  return (
    <FlatList
      data={[{key: 'a'}, {key: 'b'}, {key: 'c'}, {key: 'd'}, {key: 'e'}, {key: 'f'}, {key: 'g'}, {key: 'h'}, {key: 'i'}, {key: 'j'}]}
      renderItem={this._renderItem}
    />
  )
}
  render() {
    return (
      this.getList()
    );
  }
}

